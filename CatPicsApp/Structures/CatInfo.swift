//
//  APIResult.swift
//  CatPicsApp
//
//  Created by User on 06.11.2021.
//

import UIKit

struct CatInfo: Codable {
    var url: String
    var breeds: [BreedID]
}

struct CatPic {
    var image: UIImage
    var breeds: String?
}
