//
//  Breed.swift
//  CatPicsApp
//
//  Created by User on 12.11.2021.
//

struct Breed: Codable {
    var id: String
    var name: String
    var weight: Weight
    var life_span: String
    var description: String
    var origin: String
    var temperament: String
}

struct Weight: Codable {
    var metric: String
}

struct BreedID: Codable {
    var id: String?
}
