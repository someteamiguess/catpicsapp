//
//  Category.swift
//  CatPicsApp
//
//  Created by User on 12.11.2021.
//

struct Category: Codable {
    var id: Int
    var name: String
}
