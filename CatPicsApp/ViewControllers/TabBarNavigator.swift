//
//  TabBarController.swift
//  CatPicsApp
//
//  Created by User on 28.10.2021.
//

import UIKit

class TabBarNavigator: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()

        let searchResultScreen = UINavigationController(rootViewController: SearchResultScreen())
        let v2 = UINavigationController(rootViewController: TableviewController())
        let v3 = UINavigationController(rootViewController: Third())

        searchResultScreen.tabBarItem = UITabBarItem(title: nil,
                                                     image: UIImage(systemName: "magnifyingglass"),
                                                     selectedImage: UIImage(systemName: "magnifyingglass"))
        v2.tabBarItem = UITabBarItem(title: nil,
                                     image: UIImage(systemName: "plus"),
                                     selectedImage: UIImage(systemName: "plus"))
        v3.tabBarItem = UITabBarItem(title: nil,
                                     image: UIImage(systemName: "heart"),
                                     selectedImage: UIImage(systemName: "heart.fill"))

        self.setViewControllers([searchResultScreen, v2, v3], animated: false)
        self.view.backgroundColor = .white
    }
}

class TableviewController: UITableViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.allowsMultipleSelectionDuringEditing = true
        tableView.setEditing(true, animated: false)
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        cell.textLabel?.text = "\(indexPath.row)"
        return cell
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("select \(indexPath.item)")
    }

    override func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        print("DEselect \(indexPath.item)")
    }
}

class Third: UIViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .purple
    }
}
