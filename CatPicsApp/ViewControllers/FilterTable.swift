//
//  FilterTable.swift
//  CatPicsApp
//
//  Created by User on 13.11.2021.
//

import UIKit

class FilterTable: UIViewController {

    var myTable: UITableView!
    var selection: String = ""
    weak var delegate: RetrieveInfo?

    let list: [String: String]
    let names: [String]
    let type: String

    init(_ list: [String: String], _ type: String) {
        self.list = list
        self.names = Array(list.keys)
        self.type = type
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        myTable = UITableView()
        myTable.delegate = self
        myTable.dataSource = self
        myTable.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        view.addSubview(myTable)
        myTable.translatesAutoresizingMaskIntoConstraints = false
        myTable.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor).isActive = true
        myTable.widthAnchor.constraint(equalTo: self.view.widthAnchor).isActive = true
        myTable.leftAnchor.constraint(equalTo: self.view.leftAnchor).isActive = true
        myTable.heightAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.heightAnchor, multiplier: 0.9).isActive = true
        createFooter()
    }

    private func createFooter() {
        let customView = UIView()
        customView.backgroundColor = UIColor.white
        self.view.addSubview(customView)
        customView.translatesAutoresizingMaskIntoConstraints = false
        customView.bottomAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.bottomAnchor).isActive = true
        customView.widthAnchor.constraint(equalTo: self.view.widthAnchor).isActive = true
        customView.leftAnchor.constraint(equalTo: self.view.leftAnchor).isActive = true
        customView.heightAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.heightAnchor, multiplier: 0.1).isActive = true
        customView.layer.cornerRadius = 18
        let button = UIButton(frame: customView.frame)
        button.backgroundColor = .white
        button.setTitle("Submit", for: .normal)
        button.setTitleColor(.black, for: .normal)
        button.addTarget(self, action: #selector(buttonAction), for: .touchUpInside)
        customView.addSubview(button)
        button.layer.cornerRadius = 18
        button.translatesAutoresizingMaskIntoConstraints = false
        button.leadingAnchor.constraint(equalTo: customView.leadingAnchor).isActive = true
        button.trailingAnchor.constraint(equalTo: customView.trailingAnchor).isActive = true
        button.topAnchor.constraint(equalTo: customView.topAnchor).isActive = true
        button.heightAnchor.constraint(equalTo: customView.heightAnchor).isActive = true
    }

    @objc func buttonAction(_ sender: UIButton!) {
        if self.type == "Breed" {
            delegate?.getSelectedBreeds(new: self.selection)
        } else {
            delegate?.getSelectedCategories(new: self.selection)
        }
        self.dismiss(animated: true, completion: nil)
    }
}

extension FilterTable: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return names.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = myTable.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        cell.textLabel?.text = names[indexPath.row]
        return cell
    }

    func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        if tableView.indexPathForSelectedRow == indexPath {
            print("deselect")
            tableView.deselectRow(at: indexPath, animated: false)
            if !self.selection.isEmpty {
                self.selection = ""
            }
            return nil
        }
        print("select")
        let cell = tableView.cellForRow(at: indexPath)
        if let text = cell?.textLabel?.text {
            self.selection = list[text]!
        }
        return indexPath
    }
}
