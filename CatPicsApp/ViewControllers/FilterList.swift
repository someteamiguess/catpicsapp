//
//  FilterList.swift
//  CatPicsApp
//
//  Created by User on 11.11.2021.
//

import UIKit

class FilterList: UITableViewController {

    var labels: [String] = ["Breed", "Category"]
    var breedList: [String: String]
    var categoriesList: [String: String]
    weak var delegate: RetrieveInfo?

    init(_ breedList: [String: String], _ categoriesList: [String: String]) {
        self.breedList = breedList
        self.categoriesList = categoriesList
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return labels.count
    }

    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        let filterName = InsetLabel(frame: cell.bounds)
        filterName.text = labels[indexPath.row]
        cell.addSubview(filterName)
        return cell
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0 {
            let tableView = FilterTable(self.breedList, labels[indexPath.row])
            tableView.delegate = self.delegate
            tableView.modalPresentationStyle = .overCurrentContext
            present(tableView, animated: true, completion: nil)
        } else {
            let tableView = FilterTable(self.categoriesList, labels[indexPath.row])
            tableView.delegate = self.delegate
            tableView.modalPresentationStyle = .fullScreen
            present(tableView, animated: true, completion: nil)
        }
    }
}

class InsetLabel: UILabel {
    private var leftInset: CGFloat = 20

    override func drawText(in rect: CGRect) {
        let insets = UIEdgeInsets(top: 0, left: leftInset, bottom: 0, right: 0)
        super.drawText(in: rect.inset(by: insets))
    }

    override var intrinsicContentSize: CGSize {
        let size = super.intrinsicContentSize
        return CGSize(width: size.width + leftInset,
                      height: size.height)
    }
}
