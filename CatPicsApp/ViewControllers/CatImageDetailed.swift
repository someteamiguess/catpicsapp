//
//  CatImageDetailed.swift
//  CatPicsApp
//
//  Created by User on 06.11.2021.
//

import UIKit

class CatImageDetailed: UIViewController {

    let imageToPresent: UIImage!
    var detailedImage: UIImageView!
    var textView: UITextView!
    let infoToDisplay: Breed?

    init(displayedImage image: UIImage, info: Breed?) {
        self.imageToPresent = image
        self.infoToDisplay = info
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .white
        self.imageSetup()
        self.textSetup()
    }

    private func imageSetup() {
        detailedImage = UIImageView()
        detailedImage.backgroundColor = .green
        self.view.addSubview(detailedImage)
        detailedImage.translatesAutoresizingMaskIntoConstraints = false
        detailedImage.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor).isActive = true
        detailedImage.heightAnchor.constraint(equalTo: self.view.widthAnchor).isActive = true
        detailedImage.leftAnchor.constraint(equalTo: self.view.leftAnchor).isActive = true
        detailedImage.widthAnchor.constraint(equalTo: self.view.widthAnchor).isActive = true
        detailedImage.image = self.imageToPresent
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.imageTapped(_:)))
        detailedImage.addGestureRecognizer(tap)
        detailedImage.isUserInteractionEnabled = true
    }

    @objc func imageTapped(_ sender: UITapGestureRecognizer?) {
        let nextWindow = ZoomableImage(displayedImage: imageToPresent)
        nextWindow.modalPresentationStyle = .fullScreen
        nextWindow.modalTransitionStyle = .crossDissolve
        present(nextWindow, animated: true, completion: nil)
    }

    private func textSetup() {
        textView = UITextView()
        var text = ""
        if let info = infoToDisplay {
            text = "Breed: \(info.name)\n"
            text += "\(info.description)\n"
            text += "Life span: \(info.life_span)\n"
            text += "Weight: \(info.weight.metric)"
        } else {
            text = "No Info"
        }
        textView.font = UIFont.systemFont(ofSize: 18)
        textView.text = text
        view.addSubview(textView)
        textView.translatesAutoresizingMaskIntoConstraints = false
        textView.topAnchor.constraint(equalTo: detailedImage.bottomAnchor).isActive = true
        textView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor).isActive = true
        textView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        textView.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
    }
}
