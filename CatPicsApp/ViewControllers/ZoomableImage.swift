//
//  ZoomableImage.swift
//  CatPicsApp
//
//  Created by User on 13.11.2021.
//

import UIKit

class ZoomableImage: UIViewController {

    var scrollView = UIScrollView()
    var imageView = UIImageView()

    private let closeButton: UIButton = {
        let button = UIButton(frame: CGRect(x: 0, y: 0, width: 60, height: 60))
        button.layer.masksToBounds = true
        button.setImage(UIImage(systemName: "arrowshape.turn.up.left.fill"), for: .normal)
        button.tintColor = .white
        button.backgroundColor = .red
        return button
    }()

    init(displayedImage image: UIImage) {
        self.imageView = UIImageView(image: image)
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        view.backgroundColor = .black
        setupScrollView()
        setupImageView()
        setupCloseButton()
        view.addSubview(closeButton)
    }

    @objc private func closeZoom() {
        self.dismiss(animated: true, completion: nil)
    }

    private func setupScrollView() {
        view.addSubview(scrollView)
        scrollView.frame = view.bounds
        scrollView.center = view.center
        scrollView.maximumZoomScale = 4
        scrollView.minimumZoomScale = 1
        scrollView.delegate = self
    }

    private func setupImageView() {
        imageView.contentMode = .scaleAspectFit
        imageView.layer.masksToBounds = true
        imageView.frame = scrollView.bounds
//        imageView.frame = scrollView.bounds
        imageView.center = scrollView.center
        scrollView.addSubview(imageView)
    }

    private func setupCloseButton() {
        closeButton.addTarget(self, action: #selector(closeZoom), for: .touchUpInside)
        closeButton.frame = CGRect(x: view.safeAreaInsets.left + 10, y: view.safeAreaInsets.top + 70, width: 60, height: 60)
        view.addSubview(closeButton)
    }
}

extension ZoomableImage: UIScrollViewDelegate {
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return imageView
    }
}
