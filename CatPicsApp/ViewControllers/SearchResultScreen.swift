//
//  ViewController.swift
//  CatPicsApp
//
//  Created by Hyorinmaru on 01.10.2021.
//

import UIKit

class SearchResultScreen: UIViewController, RetrieveInfo {

    var data: [Int: CatPic] = [:]
    var breedList: [String: Breed] = [:]
    var breedNames: [String: String] = [:]
    var categoriesList: [String: String] = [:]
    var imageList: UICollectionView!

    var selectedBreeds: String = ""
    var selectedCategories: String = ""
    var page: Int = 1

    override func viewDidLoad() {
        super.viewDidLoad()
        getListsFromAPI()
        imageListInit()
        self.view.addSubview(imageList)
        self.view.addSubview(filterButton)
        getImages()
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        imageList.frame = CGRect(x: view.safeAreaInsets.left,
                                 y: view.safeAreaInsets.top,
                                 width: view.safeAreaLayoutGuide.layoutFrame.width,
                                 height: view.safeAreaLayoutGuide.layoutFrame.height)

        filterButton.translatesAutoresizingMaskIntoConstraints = false
        filterButton.bottomAnchor.constraint(equalTo: imageList.bottomAnchor, constant: -10).isActive = true
        filterButton.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -10).isActive = true
        filterButton.topAnchor.constraint(equalTo: filterButton.bottomAnchor, constant: -60).isActive = true
        filterButton.leftAnchor.constraint(equalTo: filterButton.rightAnchor, constant: -60).isActive = true
    }

    private let filterButton: UIButton = {
        let button = UIButton(frame: CGRect(x: 0, y: 0, width: 60, height: 60))
        button.layer.masksToBounds = true
        button.layer.cornerRadius = 30
        button.backgroundColor = .yellow
        button.addTarget(self, action: #selector(bringFilters), for: .touchUpInside)
        return button
    }()

    private func imageListInit() {
        let imageListLayOut: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        imageListLayOut.itemSize = CGSize(width: self.view.frame.width, height: self.view.frame.width)
        imageListLayOut.scrollDirection = .vertical
        imageList = UICollectionView(frame: self.view.safeAreaLayoutGuide.layoutFrame, collectionViewLayout: imageListLayOut)
        imageList.dataSource = self
        imageList.delegate = self
        imageList.register(UICollectionViewCell.self, forCellWithReuseIdentifier: "image")
        imageList.alwaysBounceVertical = true
    }

    @objc private func bringFilters() {
        let filterList = FilterList(self.breedNames, self.categoriesList)
        filterList.delegate = self
        navigationController?.pushViewController(filterList, animated: true)
    }

    func getListsFromAPI() {
        for (value) in ["breeds", "categories"] {
            guard let url = URL(string: "https://api.thecatapi.com/v1/\(value)/") else { return }
            let task = URLSession.shared.dataTask(with: url) { (data, _, error) in
                DispatchQueue.main.async {
                    if error != nil {
                        print(error.debugDescription)
                    } else if data != nil {
                        do {
                            if value == "breeds" {
                                let list = try JSONDecoder().decode([Breed].self, from: data!)
                                for (value) in list {
                                    self.breedList.updateValue(value, forKey: value.id)
                                    self.breedNames.updateValue(value.id, forKey: value.name)
                                }
                            } else {
                                let list = try JSONDecoder().decode([Category].self, from: data!)
                                for (value) in list {
                                    self.categoriesList.updateValue(String(value.id), forKey: value.name)
                                }
                            }
                        } catch let error {
                            print(error.localizedDescription)
                        }
                    }
                }
            }
            task.resume()
        }
    }

    func getImages() {
        var url = URLComponents(string: "https://api.thecatapi.com/v1/images/search")

        var queryItems = [URLQueryItem(name: "limit", value: "10"),
                          URLQueryItem(name: "page", value: String(self.page)),
                          URLQueryItem(name: "mime_types", value: "jpg,png")]
        if !self.selectedBreeds.isEmpty {
            queryItems.append(URLQueryItem(name: "breed_id", value: self.selectedBreeds))
        }
        if !self.selectedCategories.isEmpty {
            queryItems.append(URLQueryItem(name: "category_ids", value: self.selectedCategories))
        }
        url?.queryItems = queryItems

        var request = URLRequest(url: (url?.url)!)
        request.httpMethod = "GET"
        request.setValue("843e21ae-0e84-4927-a22d-0e2ffee0313a", forHTTPHeaderField: "x-api-key")

        let task = URLSession.shared.dataTask(with: request) { (data, _, error) in
            DispatchQueue.main.async {
                if error != nil {
                    print(error.debugDescription)
                } else if data != nil {
                    do {
                        let currentAmount = self.data.count
                        let myData = try JSONDecoder().decode([CatInfo].self, from: data!)
                        print(myData.count)
                        for (index, element) in myData.enumerated() {
                            let cur = UIImage(named: "Cat03.jpeg")
                            let test = CatPic(image: cur!, breeds: element.breeds.isEmpty ? nil : element.breeds[0].id)
                            self.data.updateValue(test, forKey: index + currentAmount)
                            self.getImage(stringUrl: element.url, forIndex: index + currentAmount)
                        }
                        self.imageList.reloadData()
                    } catch let error {
                        print(error.localizedDescription)
                    }
                }
            }
        }
        task.resume()
    }

    private func getImage(stringUrl url: String, forIndex index: Int) {
        let imageUrl = URL(string: url)!
        URLSession.shared.dataTask(with: imageUrl, completionHandler: { (data, _, error) in
            if error != nil {
                print(error)
                return
            }
            DispatchQueue.main.async {
                if let imgData = data {
                    let downloadedImage = UIImage(data: imgData)
                    if var value = self.data[index] {
                        value.image = downloadedImage!
                        self.data.updateValue(value, forKey: index)
                        self.imageList.reloadItems(at: [IndexPath(row: index, section: 0)])
                    }
                }
            }
        }).resume()
    }

    func reloadView () {
        self.page = 1
        self.data.removeAll()
        getImages()
    }

    func getSelectedBreeds(new array: String) {
        if self.selectedBreeds != array {
            self.selectedBreeds = array
            print(self.selectedBreeds)
            reloadView()
        }
    }

    func getSelectedCategories(new array: String) {
        if self.selectedCategories != array {
            self.selectedCategories = array
            print(self.selectedCategories)
            reloadView()
        }
    }

}

extension SearchResultScreen: UICollectionViewDataSource {

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return data.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "image", for: indexPath)
        let imageView = UIImageView(frame: cell.bounds)
        let item = self.data[indexPath.item]!
        imageView.frame = cell.bounds
        imageView.image = item.image
        imageView.contentMode = .scaleAspectFill
        imageView.layer.masksToBounds = true
        cell.addSubview(imageView)
        cell.backgroundColor = .black
        return cell
    }
}

extension SearchResultScreen: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let image = self.data[indexPath.item]!.image
        if let breed = self.data[indexPath.item]?.breeds {
            let breedInfo = self.breedList[breed]
            let detailedImage = CatImageDetailed(displayedImage: image, info: breedInfo)
            navigationController?.pushViewController(detailedImage, animated: true)
        } else {
            let detailedImage = CatImageDetailed(displayedImage: image, info: nil)
            navigationController?.pushViewController(detailedImage, animated: true)
        }
    }

    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if indexPath.row == data.count - 1 {
            self.page += 1
            getImages()
        }
    }
}

protocol RetrieveInfo: AnyObject {
    func getSelectedBreeds(new: String)

    func getSelectedCategories(new: String)
}
