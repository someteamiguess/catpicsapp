//
//  APIInfoGetter.swift
//  CatPicsApp
//
//  Created by User on 12.11.2021.
//

protocol APIInfoGetter: AnyObject {
    func getSelectedBreeds(breedList: [Int: String])

    func getSelectedCategories(categoriesList: [Int: String])
}
